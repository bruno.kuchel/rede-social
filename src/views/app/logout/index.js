import styled from 'styled-components'
import { Container, Card, Button, Col, Row } from 'react-bootstrap'
import history from '../../../config/history'
import { removeToken } from '../../../config/auth'

const Logout = () => {

    const logout = () => {
        removeToken()
        history.push('/login')
    }

    return(
        <>
        <Content>
        <Container className="container-logout">
            <RowJustify>
                <Container>
                    <Row>
                    <Col md={3} lg={3}/>                                  
                                
                                <Col md={6} lg={6}>
                                    <Card className="cardLogout">
                                        <Card.Body>
                                            <Card.Title className="mb-5 mt-5">Deseja sair?</Card.Title>
                                            
                                            <Button variant="dark"  className="mb-4 mr-4" onClick={logout}> SIM </Button>      
                                            
                                            <Button variant="dark" className="mb-4" onClick={() => history.push('/')}> NÃO </Button>                                      
                                        </Card.Body>                                
                                    </Card>                                    
                                </Col>
                                <Col md={3} lg={3}/>
                    </Row>
                </Container>
            </RowJustify>
            </Container>
        </Content>
        </>
    )
}

export default Logout

const Content = styled.div`
    min-height: 93.6vh;  
    display: flex;
    align-items: center;
    background: linear-gradient(#f6874f, #d8186e);

    .container-logout {
        padding-left: 10px;
        padding-right: 10px;
    }
    
    a{
        text-decoration:none;
        color: #495057;
    }


    .h5{
        font-size: 1.60rem;
        color: #000000;
    }

    
    
    .cardLogout{
        background-color: #FFF;
        border: none;
        border-radius: 40px;
        box-shadow: 3px 5px 15px rgba(0, 0, 0, 0.5);
        
    }

       


.btn {
    background-color: #000000;
    border: none;
    border-radius: 20px;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    :hover {
        background-color: #f6874f;
    }
    

}


.icon{
    font-size: 1.2em;
    margin-right: 10px;
}
   
`

const RowJustify = styled.div`
    display:flex;
    justify-content: center;
    margin: 0px;
    padding: 0px;
    text-align: center;

    
    
`