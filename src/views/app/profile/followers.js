import CardProfile from '../../../components/cardProfile'
import { Container } from 'react-bootstrap'
import styled from 'styled-components'

const Followers = () => {

const followers = [{
    "photo" : "https://i2.wp.com/www.thingsanalytic.com/wp-content/uploads/2018/08/blank-profile.png?ssl=1",
    "name" : "Marvin o Marciano",
    "username" : "@Marvin"

},
{
    "photo" : "https://i2.wp.com/www.thingsanalytic.com/wp-content/uploads/2018/08/blank-profile.png?ssl=1",
    "name" : "Marvin o Marciano 2",
    "username" : "@Marvin"

},
{
    "photo" : "https://i2.wp.com/www.thingsanalytic.com/wp-content/uploads/2018/08/blank-profile.png?ssl=1",
    "name" : "Marvin o Marciano 3",
    "username" : "@Marvin"

},
{
    "photo" : "https://i2.wp.com/www.thingsanalytic.com/wp-content/uploads/2018/08/blank-profile.png?ssl=1",
    "name" : "Marvin o Marciano 4",
    "username" : "@Marvin"

}]



    return(
        <>
        <Container>
            <RowFollower>
            {followers.map((flw, i) => (
                <CardProfile  
                key={i}
                photo={flw.photo}
                name={flw.name}
                username={flw.username}
                />               
                ))}
            </RowFollower>
        </Container>
            
        </>
    
        )    
}

export default Followers


const RowFollower = styled.div`
margin-top: 1rem;
display: grid;
grid-gap: 1rem;
grid-template-columns: repeat(auto-fill,minmax(250px,1fr));

`
