import { useEffect, useState } from 'react'
import history from '../../../config/history'
import { Container, Row, Col, Image, Tabs, Tab, Button} from 'react-bootstrap'
import styled from 'styled-components'
import { MdLocationOn } from 'react-icons/md'
import { BiEdit } from 'react-icons/bi'
import { getUser } from '../../../config/auth'
import Posts from './posts'
import Followers from './followers'
import Following from './following'
import { me } from '../../../services/user'


const Profile = () => {

    const [userProfile, setUserProfile] = useState({})
    
    const _id = getUser().id

    useEffect(() => {

        let get = async () => {
          //setIsLoading(true)
          const user = await me(_id)
          setUserProfile(user.data)
          //setIsLoading(false)
        }
    
        get()
    
        return () => get = () => {
        }
    
      }, [_id])
    


    return(
        <>
        
        <Container className="mt-4">
            <RowHeader>
                <Col md={4} lg={4} className="mb-4">
                <PhotoProfile> 
                <Image src="https://i2.wp.com/www.thingsanalytic.com/wp-content/uploads/2018/08/blank-profile.png?ssl=1" roundedCircle  />
                </PhotoProfile>
                </Col>
                <Col md={8} lg={8}>
                <InfoProfile>
                    <Information>
                    <Name>{userProfile.name}</Name>
                    <Username>{userProfile.username}</Username>
                    <Location>
                    <MdLocationOn className="icon"/> {userProfile.place}
                    </Location>
                    <ActionProfile>
                        <Button variant="dark" onClick={() =>  history.push('/edit-profile') }><BiEdit className="icon-btn" />Editar</Button>
                    </ActionProfile>
                    </Information>
                    
                    </InfoProfile>
                </Col>
            </RowHeader>            
            <RowTabs defaultActiveKey="posts" className="mt-4" >
  <Tab eventKey="posts" title="Posts" className="my-tab">
    <Posts/>
  </Tab>
  <Tab eventKey="followers" title="Followers">
    <Followers/>
  </Tab>
  <Tab eventKey="following" title="Following" >
    <Following/>
  </Tab>
</RowTabs>
        </Container>
        
        </>        
    )

}

export default Profile

const RowHeader = styled(Row)`
min-height: 200px;

`


const PhotoProfile = styled.div`
text-align: center;
align-items: center;


    img {
        width: 200px;
        height: 200px;
    }

`
const InfoProfile = styled.div`
justify-content: center;
display: flex;
height: 100%;
align-items: center; 
`

const Information = styled.div`


`

const ActionProfile = styled.div`
text-align: right;

.btn {
    padding-right: 1rem; 
    padding-left: 1rem; 
    font-size: 1.2rem;
    font-weight: bold;
    background-color: #000000;
    border: none;
    border-radius: 20px;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    :hover {
        background-color: #f6874f;
    }

.icon-btn {
    font-size: 1.5rem;
    margin-right: 0.5rem;
    margin-bottom:0.2rem;
    
}
    

}

`

const Name = styled.h1`
color: #d8186e;
font-size: 2.3rem;
font-weight: 800;

`

const Username = styled.h4`
color: #C1C1C1;
font-size: 1.5rem;

`

const Location = styled.div`
font-size: 1.5rem;
color: #C1C1C1;
    .icon{
        font-size: 1.5rem;
        color: #C1C1C1;
    }
`

const RowTabs = styled(Tabs)`


    
    
a{
    color: #f6874f;
    font-size: 1.1rem;
    font-weight: bold;     
    } 
    
    




`