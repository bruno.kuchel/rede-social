import { useState } from 'react'
import { editUser } from '../../../services/user'
import styled from 'styled-components'
import { Container, Card, InputGroup, FormControl, Button, Col, Row } from 'react-bootstrap'
import { FaLock, FaUser } from 'react-icons/fa'
import { MdEmail, MdPlace } from 'react-icons/md'
import { ImProfile } from 'react-icons/im'
import history from '../../../config/history'
import { removeToken } from '../../../config/auth'

const EditProfile = () => {

    const [form, setForm] = useState({})
   


    const handleChange = (attr) => {
        setForm({
            ...form,
            [attr.target.name] : attr.target.value            
        })
    }

    const isFormValid = () => 
    form.email 
    && form.name
    && form.place
    && form.username 
    && form.password
    
    

    const submitLogin = async () => {
        if (isFormValid()) {
            try {            
                await editUser(form)
                setForm({})
                console.log('usuario criado')
            } catch (error) {
                console.log('erro')
            }
                
        }
        
    } 

    const pressEnter = (event) => event.key === 'Enter' ? submitLogin() : null

    const logout = () => {
        removeToken()
        history.push('/login')
    }

    return(
        <>
        <Content>
        <Container className="container-edit">
            <RowJustify>
                <Container>
                    <Row>
                    <Col md={3} lg={3}/>                                  
                                
                                <Col md={6} lg={6}>
                                    <Card className="cardEdit">
                                        <Card.Body>
                                        <Card.Title className="mb-5 mt-5">Editar perfil</Card.Title>
                                            <InputGroup className="mb-4">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text className="prependBg"><MdEmail /></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl type="email" name="email" className="input" placeholder="E-mail" onChange={handleChange} value={form.email || ""} onKeyPress={pressEnter}/>
                                                </InputGroup>
                                                <InputGroup className="mb-4">
                                                    <InputGroup.Prepend>
                                                        <InputGroup.Text className="prependBg"><ImProfile /></InputGroup.Text>
                                                    </InputGroup.Prepend>
                                                <FormControl type="text" name="name" className="input" placeholder="Nome e sobrenome" onChange={handleChange} value={form.name || ""} maxLength="30" onKeyPress={pressEnter}/>
                                            </InputGroup>
                                            <InputGroup className="mb-4">
                                                    <InputGroup.Prepend>
                                                        <InputGroup.Text className="prependBg"><MdPlace /></InputGroup.Text>
                                                    </InputGroup.Prepend>
                                                <FormControl type="text" name="place" className="input" placeholder="Localidade" onChange={handleChange} value={form.place || ""} maxLength="30" onKeyPress={pressEnter}/>
                                            </InputGroup>                                              
                                            <InputGroup className="mb-4">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text className="prependBg"><FaUser /></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl type="text" name="username" className="input" placeholder="@username" onChange={handleChange} value={form.username || ""} maxLength="20" onKeyPress={pressEnter}/>
                                            </InputGroup>
                                            <InputGroup className="mb-5">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text className="prependBg"><FaLock /></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl type="password" name="password" className="input" placeholder="Password" onChange={handleChange} value={form.password || ""} onKeyPress={pressEnter}/>
                                            </InputGroup>
                                            
                                            <Button variant="dark"  className="mb-4 mr-4" onClick={logout}> Salvar </Button>      
                                            
                                            <Button variant="dark" className="mb-4" onClick={() => history.push('/me')}> Cancelar </Button>                                      
                                        </Card.Body>                                
                                    </Card>                                    
                                </Col>
                                <Col md={3} lg={3}/>
                    </Row>
                </Container>
            </RowJustify>
            </Container>
        </Content>
        </>
    )
}

export default EditProfile

const Content = styled.div`
    min-height: 93.6vh;  
    display: flex;
    align-items: center;
    background: linear-gradient(#f6874f, #d8186e);

    .container-edit {
        padding-left: 10px;
        padding-right: 10px;
    }
    
    a{
        text-decoration:none;
        color: #495057;
    }


    .h5{
        font-size: 1.60rem;
        color: #000000;
    }

    
    
    .cardEdit{
        background-color: #FFF;
        border: none;
        border-radius: 40px;
        box-shadow: 3px 5px 15px rgba(0, 0, 0, 0.5);
        
    }

       


    .prependBg{
        background-color: #000000;
        border: none;
        border-radius: 0px;
        font-size: 1em;
        color: #FFF;
    }

    

.input{
    background-color: #E8E8E8;
    font-size: 1em;
    border: none;
    border-radius: 0px;
    padding: 15px;
    
          
}

.btn {
    background-color: #000000;
    border: none;
    border-radius: 20px;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    :hover {
        background-color: #f6874f;
    }
    :disabled {
        background-color: #000000;
        cursor: not-allowed;
    }

}


.icon{
    font-size: 1.2em;
    margin-right: 10px;
}
   
`

const RowJustify = styled.div`
    display:flex;
    justify-content: center;
    margin: 0px;
    padding: 0px;
    text-align: center;

    
    
`