import { Col, Container, Row } from 'react-bootstrap'
import Post from '../../../components/post'

const Posts = () => {

    const item = [{
        "photo" : "https://i2.wp.com/www.thingsanalytic.com/wp-content/uploads/2018/08/blank-profile.png?ssl=1",
        "name" : "Marvin o Marciano",
        "username" : "@Marvin",
        "text" : "Um morcego, um homem com máscara e a humanidade em perigo. Batman Te Dark Knight Rises = premonição do Coronavirus.",
        "image" : "https://pbs.twimg.com/media/EQ68SxdX0AIlodg?format=jpg&name=small",
        "created_at" : "Today"        
    },
    {
        "photo" : "https://i2.wp.com/www.thingsanalytic.com/wp-content/uploads/2018/08/blank-profile.png?ssl=1",
        "name" : "Marvin o Marciano",
        "username" : "@Marvin",
        "text" : "Some quick example text to build on the card title and make up the bulk of the card's content.",
        "image" : "",
        "created_at" : "Yesterday"    
    },
    {
        "photo" : "https://i2.wp.com/www.thingsanalytic.com/wp-content/uploads/2018/08/blank-profile.png?ssl=1",
        "name" : "Marvin o Marciano",
        "username" : "@Marvin",
        "text" : "Some quick example text to build on the card title and make up the bulk of the card's content.",
        "image" : "https://estaticos.efe.com/efecom/recursos2/imagen.aspx?-P-2fL4Jfo8HOMhrFyb8WspSaEeuhTp8mll-P-2fQ4TncnkXVSTX-P-2bAoG0sxzXPZPAk5l-P-2fU5UUfuHZusz3R-P-2byrknh5YeSAA-P-3d-P-3d",
        "created_at" : "2 days ago"    
    },]

    return (
        <>
            <Container className="mt-4">
                <Row>
                    <Col sm={0} md={2} lg={2} />
                    <Col sm={12} md={8} lg={8}>
                        { item.map((pst, i) => (
                            <Post 
                            key={i}
                            photo={pst.photo}
                            name={pst.name}
                            username={pst.username}
                            text={pst.text}
                            image={pst.image}
                            created_at={pst.created_at}
                            
                            />
                        ))}
                        
                    </Col>
                    <Col sm={0} md={2} lg={2} />
                </Row>
            </Container>

        </>

    )
}

export default Posts