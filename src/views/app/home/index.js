import Feed from './feed'
import FormPost from './form'
import styled from 'styled-components'

import { Container } from 'react-bootstrap'

const Home = () => {
    return(
        <Content>
            <FormPost/>
        <Feed/>
        </Content>
    )
}

export default Home

const Content = styled(Container)`
padding-top: 1.5rem;
min-height: 93.6vh;
border-left: 1px solid rgba(0,0,0,.125);
border-right: 1px solid rgba(0,0,0,.125);
`