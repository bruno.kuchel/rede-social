import { Button, Row, Col, Form, Dropdown } from 'react-bootstrap'
import styled from 'styled-components'
import { FiSend } from 'react-icons/fi'

const FormPost = () => {
    return (
        <>

            <Row className="mb-4">
                <Col sm={0} md={2} lg={2} />
                <Col sm={12} md={8} lg={8}>
                    <CardForm>
                        
                            <Form.Group controlId="exampleForm.ControlTextarea1">                                
                                <Form.Control as="textarea" rows={3} placeholder="Escreve alguma bobagem..." />
                            </Form.Group>

                            <Dropdown.Divider />
                            
                            <ActionCard>
                                <Button variant="dark"><FiSend className="icon-btn" />Postar</Button>
                            </ActionCard>
                        
                    </CardForm>

                </Col>
                <Col sm={0} md={2} lg={2} />
            </Row>


        </>
    )
}

export default FormPost




const CardForm = styled.div`
padding: 1.25rem;
text-align: center;
border-radius: 0.7rem;
border: 1px solid rgba(0,0,0,.125);
    .card-title {
        margin-bottom: 0.1rem;
    }
    .form-control {
    border: none;
    background-color: none;

    }


`


//const PhotoProfile = styled(Image)`    
//    width: 50px;
//    height: 50px;   

//`

const ActionCard = styled.div`
text-align: right;

.btn {
    padding-right: 1rem; 
    padding-left: 1rem; 
    font-size: 1.2rem;
    font-weight: bold;
    background-color: #000000;
    border: none;
    border-radius: 20px;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    :hover {
        background-color: #f6874f;
    }

.icon-btn {
    font-size: 1.5rem;
    margin-right: 0.5rem;
    margin-bottom:0.2rem;
    
}
    

}

`