import { useState } from 'react'
import { newUser } from '../../services/user'
import styled from 'styled-components'
import history from '../../config/history'



import { Container, Card, InputGroup, FormControl, Button, Col, Row } from 'react-bootstrap'
import { FaLock, FaUser } from 'react-icons/fa'
import { MdEmail, MdPlace } from 'react-icons/md'
import { ImProfile } from 'react-icons/im'






const Signup = () => {
    const [form, setForm] = useState({})
   


    const handleChange = (attr) => {
        setForm({
            ...form,
            [attr.target.name] : attr.target.value            
        })
    }

    const isFormValid = () => 
    form.email 
    && form.name
    && form.place
    && form.username 
    && form.password
    
    

    const submitLogin = async () => {
        if (isFormValid()) {
            try {            
                await newUser(form)
                setForm({})                
                console.log('usuario criado')
                history.push('/')
            } catch (error) {
                console.log('erro')
            }
                
        }
        
    } 

    const pressEnter = (event) => event.key === 'Enter' ? submitLogin() : null


    return (
        <>
            <Content>
                <Container className="container-form">
                    <RowJustify>
                        <Container>
                            <Row>
                            
                                <Col md={5} lg={5}>
                                    <Card className="cardLogo">
                                        <Logo> Logo Rede Social</Logo>
                                    </Card>
                                </Col>
                                <Col md={6} lg={5}>
                                    <Card className="cardForm">
                                        <Card.Body>
                                            <Card.Title className="mb-5 mt-5">Cadastre-se</Card.Title>
                                            <InputGroup className="mb-4">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text className="prependBg"><MdEmail /></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl type="email" name="email" className="input" placeholder="E-mail" onChange={handleChange} value={form.email || ""} onKeyPress={pressEnter}/>
                                                </InputGroup>
                                                <InputGroup className="mb-4">
                                                    <InputGroup.Prepend>
                                                        <InputGroup.Text className="prependBg"><ImProfile /></InputGroup.Text>
                                                    </InputGroup.Prepend>
                                                <FormControl type="text" name="name" className="input" placeholder="Nome e sobrenome" onChange={handleChange} value={form.name || ""} maxLength="30" onKeyPress={pressEnter}/>
                                            </InputGroup>
                                            <InputGroup className="mb-4">
                                                    <InputGroup.Prepend>
                                                        <InputGroup.Text className="prependBg"><MdPlace /></InputGroup.Text>
                                                    </InputGroup.Prepend>
                                                <FormControl type="text" name="place" className="input" placeholder="Localidade" onChange={handleChange} value={form.place || ""} maxLength="30" onKeyPress={pressEnter}/>
                                            </InputGroup>                                              
                                            <InputGroup className="mb-4">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text className="prependBg"><FaUser /></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl type="text" name="username" className="input" placeholder="@username" onChange={handleChange} value={form.username || ""} maxLength="20" onKeyPress={pressEnter}/>
                                            </InputGroup>
                                            <InputGroup className="mb-5">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text className="prependBg"><FaLock /></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl type="password" name="password" className="input" placeholder="Password" onChange={handleChange} value={form.password || ""} onKeyPress={pressEnter}/>
                                            </InputGroup>
                                            <Button variant="dark" block className="mb-4" onClick={submitLogin} disabled={!isFormValid()}> Enviar </Button>      
                                                                              
                                        </Card.Body>                                      

     
                                    </Card>
                                    
                                </Col>
                                
                            </Row>
                        </Container>
                    </RowJustify>
                    
                </Container>
            </Content>
        </>
    )
}



export default Signup

const Content = styled.div`
    min-height: 100vh;
    display: flex;
    align-items: center;
    background: linear-gradient(#f6874f, #d8186e);

    .container-form {
        padding-left: 10px;
        padding-right: 10px;
    }
    
    a{
        text-decoration:none;
        color: #495057;
    }


    .h5{
        font-size: 1.60rem;
        color: #000000;
    }

    .cardLogo{
        height: 100%;        
        padding-bottom: 20px;
        background-color: transparent;
        display: flex;
        align-items: center;
        justify-content: center;
        border: none;
        
        
    }
    
    .cardForm{
        background-color: #FFF;
        border: none;
        border-radius: 40px;
        box-shadow: 3px 5px 15px rgba(0, 0, 0, 0.5);
        
    }

   
    .prependBg{
        background-color: #000000;
        border: none;
        border-radius: 0px;
        font-size: 1em;
        color: #FFF;
    }

    

.input{
    background-color: #E8E8E8;
    font-size: 1em;
    border: none;
    border-radius: 0px;
    padding: 15px;
    
          
}

.btn {
    background-color: #000000;
    border: none;
    border-radius: 20px;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    :hover {
        background-color: #f6874f;
    }
    :disabled {
        background-color: #000000;
        cursor: not-allowed;
    }

}


.icon{
    font-size: 1.2em;
    margin-right: 10px;
}
   
`

const Logo = styled.div`
text-align: center;
font-size: 1.2em;
color: #FFF;
`


const RowJustify = styled.div`
    display:flex;
    justify-content: center;
    margin: 0px;
    padding: 0px;
    text-align: center;

    
    
`
