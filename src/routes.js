import { Router, Route, Switch, Redirect } from 'react-router-dom'
import history from './config/history'
import { isAuthenticated } from './config/auth'

import Layout from './components/layout'
import Login from './views/login'
import Signup from './views/signup'
import Home from './views/app/home'
import Profile from './views/app/profile'
import EditProfile from './views/app/profile/edit'
import Logout from './views/app/logout'


import Error404 from './404'

const AuthRoute = ({ ...rest }) => {

    if (!isAuthenticated()) {
         return <Redirect to='/login' />
     }
     return <Route  {...rest} />
 } 


const Routes = () => (
    <Router history={history}> 
        <Switch>
            
            
            
            <Route path="/login" component={Login}/>
            <Route path="/signup" component={Signup}/>
            
            
            
            <Layout>
            
            <AuthRoute path="/me" component={Profile}/>
            <AuthRoute path="/edit-profile" component={EditProfile}/>
            <AuthRoute exact path="/" component={Home}/> 
            <AuthRoute exact path="/logout" component={Logout}/>            
            </Layout>
            <Route path="*" component={Error404} />
            
           
            
            
           

        </Switch>
    
    </Router>
)

export default Routes


