import { applyMiddleware, combineReducers, createStore } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunk from "redux-thunk"
import multi from "redux-multi"

//import { reducer as toastrReducer } from "react-redux-toastr";

import LoginReducer from './Login/login.reducer';
//import PostReducer from "./Post/post.reducer";
//import UserReducer from "./User/user.reducer";

// modularizações 
const reducers = combineReducers({
  
  auth: LoginReducer
  //post: PostReducer,
  //user: UserReducer,
 // toastr: toastrReducer, // <- Mounted at toastr.
});

// middlewares 
const middleware = [thunk, multi]

// compose
const compose = composeWithDevTools(applyMiddleware(...middleware))

// criação da store
const store = createStore(reducers, compose)

export default store

