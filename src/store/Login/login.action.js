import { saveToken } from "../../config/auth";
import history from "../../config/history";
import  http  from "../../config/http";
import { authentication } from "../../services/auth";
//import { toastr } from 'react-redux-toastr';

export const SIGN = "SIGN";
export const SIGN_LOADING = "SIGN_LOADING";

export const login = (props) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SIGN_LOADING, loading: true });
      const { data } = await authentication(props);
      dispatch({ type: SIGN, data: data });
      saveToken(data);
      http.defaults.headers["x-auth-token"] = data.token;
      history.push("/");
      dispatch({ type: SIGN_LOADING, loading: false })

    } catch (error) {
      //toastr.error("ERROR !", `Não foi possível fazer o login, verifique login e senha`);
        console.log('Ocorreu um erro')
        dispatch({ type: SIGN_LOADING, loading: false })
    }
  };
};
