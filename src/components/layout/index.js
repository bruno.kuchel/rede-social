import styled from 'styled-components'

import Header from './header'



const Layout = ({ children }) =>{
    return (
        <>
        <Content>
        <Header/>
        <Main>
            { children }
        </Main>
        
        
        </Content>
        </>

    )
}

export default Layout

const Content = styled.div`
min-height: 100vh;
display: flex;
flex-direction: column;
`


const Main = styled.div`
    margin-top: 62px;
    flex: 1 0 auto;
    min-height: 400px;
    background: #FFFFFF;
`
