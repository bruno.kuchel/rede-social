import styled from 'styled-components'
import { Nav, Navbar, Container } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import { RiHome7Fill, RiUserFill, RiLogoutCircleRLine } from 'react-icons/ri'


const Header = () => {

    const menu = [
        {
            name: "Home",
            key: "1",
            link: "/",
            icon: <RiHome7Fill/>
        },
        {
            name: "Perfil",
            key: "2",
            link: "/me",
            icon: <RiUserFill/>
        },
        {
            name: "Sair",
            key: "3",
            link: "/logout",
            icon: <RiLogoutCircleRLine/>
        }
        
        
    ]
    return (
        <Bar>
            
            <Navbar collapseOnSelect expand="lg" variant="dark" fixed="top">
            <Container>
                <NavLink to={'/'}>
                <Navbar.Brand>Rede Social</Navbar.Brand>
                </NavLink>
  
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
    <Nav>
        {menu.map((item, i) => (
            <NavLink exact={true} to={item.link} key={i} >
                <Nav.Link as="div" eventKey={item.key}>
                    <Icon>{item.icon}</Icon>
                    <Item>{item.name}</Item>
                    </Nav.Link>
            </NavLink>
            

        ))}
      
      
    </Nav>    
  </Navbar.Collapse>
  </Container>
</Navbar>

        </Bar>

    )
}

const Bar = styled.div`
    background-color: #f6874f;
    a {
        text-decoration: none;
               
    }

    .navbar{
        background-color: #f6874f;
    }

    .navbar-brand{
        color: #FFF;
        font-size: 1.5em;
    }
    
    .navbar-toggler{
        padding: 0px;
        border: none;

              
    }

    
    .navbar-toggler:focus{
        outline: none;
        border: none;        
    }
    

    .nav-link{
        margin-left: 0.5em;
        font-size: 1.2em;   
        font-weight: bold;  
        display: flex;
                   

        :hover{
        color: #000000 !important; 
        font-weight: bold;
        
        }            
    }  
    
    .navbar-nav .active .nav-link {
        color: #000000;
        font-weight:bold;
    }

    .navbar-nav .nav-link{
        color: #FFF;
    }

    .navbar-toggler-icon {
        
        background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(255,255,255, 1)' stroke-width='2' stroke-linecap='square' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E");
}

`

const Icon = styled.div`
margin-right: 0.5rem;
`
const Item = styled.div`
    padding-top: 0.1rem;
`

export default Header
