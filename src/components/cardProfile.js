import { Image, Card, Button } from 'react-bootstrap'
import styled from 'styled-components'
import { BiUserPlus } from 'react-icons/bi'

const Following = ({
    photo,
    name,
    username
}) => {
    return (
        <>
            <CardProfile>
                <Card.Body>
                    <PhotoProfile roundedCircle src={photo} className="mb-2" />
                    <Card.Title>{name}</Card.Title>
                    <Card.Text>
                        {username}
                    </Card.Text>
                    <ActionCard>
                    <Button variant="dark"><BiUserPlus className="icon-btn"/>Follow</Button>
                    </ActionCard>
                </Card.Body>
            </CardProfile>
        </>
    )
}

export default Following




const CardProfile = styled(Card)`
//width: 25rem;
text-align: center;
border-radius: 0.7rem;
//border: none;
    .card-title {
        margin-bottom: 0.1rem;
    }

`

const PhotoProfile = styled(Image)`    
    width: 50px;
    height: 50px;   

`

const ActionCard = styled.div`


.btn {
    padding-right: 1rem; 
    padding-left: 1rem; 
    font-size: 1.2rem;
    font-weight: bold;
    background-color: #000000;
    border: none;
    border-radius: 20px;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    :hover {
        background-color: #f6874f;
    }

.icon-btn {
    font-size: 1.5rem;
    margin-right: 0.5rem;
    margin-bottom:0.2rem;
    
}
    

}

`