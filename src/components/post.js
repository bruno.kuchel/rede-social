import { Row, Col, Image, Card } from 'react-bootstrap'
import styled from 'styled-components'

const Post = ({
        photo,
        name,
        username,
        text,
        image,
        created_at
}) => {

    return (
        <>

            <RowPost>
                <CardPost className="mb-4">

                    <Card.Body>
                        <Row>
                            <Col sm={2} md={2} lg={2}>
                                <PhotoProfile src={photo} roundedCircle className="mb-3" />

                            </Col>
                            <Col sm={10} md={10} lg={10}>
                                <Card.Title>{name}</Card.Title>
                                <Card.Text>{username}</Card.Text>
                                <Card.Text>{text}</Card.Text>
                                <Card.Img variant="top" src={image} />
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer className="text-muted">{created_at}</Card.Footer>
                </CardPost>
            </RowPost>


        </>

    )
}

export default Post

const RowPost = styled(Row)`

`
const CardPost = styled(Card)`

border-radius: 0.7rem;
//border: none;
    .card-title {
        margin-bottom: 0.1rem;
    }

`

const PhotoProfile = styled(Image)`    
    width: 50px;
    height: 50px;   

`
