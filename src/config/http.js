import axios from "axios";
import { getToken } from "./auth";

const localUrlApi = 'https://agile-dusk-91712.herokuapp.com'



const http = axios.create({
  baseURL: process.env.NODE_ENV ==='development'
  ? localUrlApi
  : process.env.REACT_APP_API

});


http.defaults.headers["Content-type"] = "application/json";


if (getToken()) {
  http.defaults.headers["x-auth-token"] = getToken();
}

export default http

