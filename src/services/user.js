import http from '../config/http'

const newUser = (data) => http.post(`/user`, data)
const editUser = (data) => http.patch(`/user`, data)

const me = (id) => http.get(`/user/${id}`)

export {
    newUser,
    editUser,
    me
}
